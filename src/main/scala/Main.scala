import scala.util.Try
import scala.util.Success
import scala.util.Failure
import ParseInt.toInt
object Main extends App {

  // Custom types
  type ParseNumberType = Int.type | Double.type | Float.type

  class ParseToNumber {
    def parseTo(value: String, numberType: ParseNumberType): Int | Double |
      Float = {
      numberType match
        case Int => {
          toInt(value, 10) match {
            case Success(value)     => value
            case Failure(exception) => 0
          }
        }
        case Double => 0.0
        case Float  => 0.0f
    }
  }

  val s = new ParseToNumber
  val f = s.parseTo("-82", Int)
  print(f)
}
