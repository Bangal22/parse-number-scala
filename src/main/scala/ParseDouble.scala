import scala.util.Try
import scala.util.Success
import ParseInt.toInt
import scala.util.Failure

object ParseDouble extends SignTrait with getIntTrait {
  def toDouble(value: String, radix: Int = 10): Try[Double] = {
    if (value.isEmpty())
      throw new NumberFormatException("The input value is empty")

    if (value.trim().length() < 0)
      throw new NumberFormatException(s"Invalid character $value")

    val numbers_separated = value.split("\\.").toList

    if (numbers_separated.length == 2) {
      if (hasSign(numbers_separated(1).charAt(0)))
        throw new NumberFormatException(s"Invalid character $value")
      Success(
        getDoubleNumber(
          getInt(numbers_separated(0)),
          getInt(numbers_separated(1)),
          radix
        )
      )
    } else Success(getDoubleNumber(getInt(numbers_separated(0)), 0, radix))
  }

  def getDoubleNumber(n1: Int, n2: Int, radix: Int): Double =
    n1 + (n2 / math.pow(radix, n2.toString().length()))
}
