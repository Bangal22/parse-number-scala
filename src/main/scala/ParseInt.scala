import scala.util.Try
import scala.util.Success
import scala.util.Failure
import scala.compiletime.ops.boolean

object ParseInt extends SignTrait {

  val MIN_RADIX = 2
  val MAX_RADIX = 36
  val NEGATIVE_SIGN = '-'
  val POSITIVE_SIGN = '+'

  def toInt(value: String, radix: Int = 10): Try[Int] = {
    if (value.isEmpty())
      throw new NumberFormatException("The input value is empty")

    if (radix > MAX_RADIX)
      throw new NumberFormatException(s"radix $radix less than MIN_RADIX")

    if (radix < MIN_RADIX)
      throw new NumberFormatException(s"radix $radix greater than MAX_RADIX")

    if (value.trim().length() < 0)
      throw new NumberFormatException(s"Invalid character $value")

    val sign = getSign(value.charAt(0)) match {
      case Some(value) => value
      case None => throw new NumberFormatException(s"Invalid character $value")
    }

    toIntReducer(0, value.toList, true, radix) match {
      case Some(digit) => Success(digit * sign)
      case None => throw new NumberFormatException(s"Invalid character $value")
    }
  }

  def toIntReducer(
      accumulator: Int,
      element: List[Char],
      firstValue: Boolean,
      radix: Int
  ): Option[Int] = element match {
    case head :: next => {
      if (firstValue && (head == NEGATIVE_SIGN || head == POSITIVE_SIGN))
        return toIntReducer(accumulator, next, false, radix)
      if (!head.isDigit)
        return None
      toIntReducer(
        (accumulator * radix) + (head - '0'),
        next,
        false,
        radix
      )
    }
    case Nil => Some(accumulator)
  }

}
