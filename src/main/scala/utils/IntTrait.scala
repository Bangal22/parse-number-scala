import scala.util.Try
import scala.util.Success
import ParseInt.toInt
import scala.util.Failure

trait getIntTrait {
  def getInt(value: String): Int = toInt(value) match {
    case Success(digit) => digit
    case Failure(exception) =>
      throw new NumberFormatException(exception.getMessage())
  }
}
