trait SignTrait {
  def getSign(character: Char): Option[Int] = character match {
    case '-'                            => Some(-1)
    case '+'                            => Some(1)
    case character if character.isDigit => Some(1)
    case _: Char                        => None
  }

  def hasSign(character: Char): Boolean = character match {
    case character if character.isDigit => false
    case _: Char => true
  }
}
