import Main.ParseToNumber
import org.scalatest.funspec
import munit.FunSuite
import org.scalatest.funspec.AnyFunSpec

import ParseInt.{toInt as ti, getSign as gs, toIntReducer as tir}
import ParseDouble.{toDouble as td}
import scala.util.Success
import scala.util.Failure
import org.scalatest.funspec.AsyncFunSpec

trait TraitAnyFunSpec extends funspec.AnyFunSpec

class getSign extends FunSuite {
  test("get sign with '9' should always give true") {
    val sign = gs('9') match {
      case Some(value) => value
      case None =>
        throw new NumberFormatException("Test error")
    }
    assertEquals(sign, 1)
  }

  test("get sign with '-' should always give false") {
    val sign = gs('-') match {
      case Some(value) => value
      case None =>
        throw new NumberFormatException("Test error")
    }
    assertEquals(sign, -1)
  }

  test("get sign with '+' should always give true") {
    val sign = gs('+') match {
      case Some(value) => value
      case None =>
        throw new NumberFormatException("Test error")
    }
    assertEquals(sign, 1)
  }

}

class GetSignException extends AnyFunSpec {
  describe("get a sign") {
    it(
      "should produce NumberFormatException when getSign is invoked with '=' sign"
    ) {
      assertThrows[NumberFormatException] {
        val sign = gs('=') match {
          case Some(value) => value
          case None =>
            throw new NumberFormatException("Test error")
        }
      }
    }
  }

  describe("get a sign") {
    it(
      "should produce NumberFormatException when getSign is invoked with '?' sign"
    ) {
      assertThrows[NumberFormatException] {
        val sign = gs('?') match {
          case Some(value) => value
          case None =>
            throw new NumberFormatException("Test error")
        }
      }
    }
  }

}

class ToIntReducer() extends FunSuite {
  test("to int reducer with '8)2' should always give None") {
    val digit = tir(0, List('8', ')', '2'), true, 10) match {
      case Some(value) => value
      case None        => None
    }
    assertEquals(digit, None)
  }

  test("to int reducer with '-82' should always give -82") {
    val sign = gs('-') match {
      case Some(value) => value
    }
    val digit = tir(0, List('-', '8', '2'), true, 10) match {
      case Some(value) => value * sign
      case None        => None
    }
    assertEquals(digit, -82)
  }

  test("to int reducer with '+82' should always give 82") {
    val digit = tir(0, List('+', '8', '2'), true, 10) match {
      case Some(value) => value
      case None        => None
    }
    assertEquals(digit, 82)
  }

  test("to int reducer with '=82' should always give None") {
    val digit = tir(0, List('=', '8', '2'), true, 10) match {
      case Some(value) => value
      case None        => None
    }
    assertEquals(digit, None)
  }
}

class toInt extends FunSuite {
  test("to int with '88' should always give 88") {
    val digit = ti("88", 10) match {
      case Success(value)     => value
      case Failure(exception) => 0
    }
    assertEquals(digit, 88)
  }

  test("to int with '44' should always give 44") {
    val digit = ti("44", 10) match {
      case Success(value)     => value
      case Failure(exception) => 0
    }
    assertEquals(digit, 44)
  }
}

class toIntException extends AsyncFunSpec {
  describe("get a int") {
    it(
      "should produce NumberFormatException when toInt is invoked with empty value"
    ) {
      assertThrows[NumberFormatException] {
        val digit = ti(" 88", 10) match {
          case Success(value)     => value
          case Failure(exception) => 0
        }
      }
    }
  }

  describe("get a int") {
    it(
      "should produce NumberFormatException when toInt is invoked with '*' value"
    ) {
      assertThrows[NumberFormatException] {
        val digit = ti("8*8", 10) match {
          case Success(value)     => value
          case Failure(exception) => 0
        }
      }
    }
  }
}

class toDouble extends FunSuite {
  test("to double with '88' should always give 88.0") {
    val digit = td("88", 10) match {
      case Success(value)     => value
      case Failure(exception) => 0
    }
    assertEquals(digit, 88.0)
  }

  test("to double with '4.4' should always give 4.4") {
    val digit = td("4.4", 10) match {
      case Success(value)     => value
      case Failure(exception) => 0
    }
    assertEquals(digit, 4.4)
  }
}

class toDoubleException extends AsyncFunSpec {
  describe("get a double") {
    it(
      "should produce NumberFormatException when toDouble is invoked with empty value"
    ) {
      assertThrows[NumberFormatException] {
        val digit = td(" 8-8", 10) match {
          case Success(value)     => value
          case Failure(exception) => 0
        }
      }
    }
  }

  describe("get a double") {
    it(
      "should produce NumberFormatException when toDouble is invoked with '8.-8' value"
    ) {
      assertThrows[NumberFormatException] {
        val digit = td("8.-8", 10) match {
          case Success(value)     => value
          case Failure(exception) => 0
        }
      }
    }
  }
}